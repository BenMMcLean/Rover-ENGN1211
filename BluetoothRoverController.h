#ifndef BLUETOOTHROVERCONTROLLER_H
#define BLUETOOTHROVERCONTROLLER_H

#include "Bluetooth.h"
#include "RoverController.h"
#include "Debug.h"

//All the bluetooth commands
#define BT_STOP     0x30
#define BT_FORWARD  0x38
#define BT_BACKWARD 0x32
#define BT_LEFT     0x34
#define BT_RIGHT    0x36
#define BT_R_LEFT   0x45
#define BT_R_RIGHT  0x46

/*
 * This controller allows the user to move the rover using a bluetooth app
 */
class BluetoothRoverController : public RoverController{
public:
  //The bluetooth module that provides commands
  Bluetooth * b;

  BluetoothRoverController(){}
  BluetoothRoverController(Bluetooth * b, Rover * r) : RoverController(r){
    this->b = b;
    Debug::out("Using Bluetooth Rover Controller");
  }

  bool blockForward = false;
  bool goingForward = false;

  virtual bool run(long diff){
    //Get the bluetooth buffer
    byte control = b->readOne();

    Debug::out("test");
    
    double distance = rover->scan(90,1);
    bool blockForward = distance <= 10;
    if(blockForward && goingForward){
      rover->stop();
    }
    
    //If no new command has been issued, exit function
    if(control == '\0'){
      return;
    }
    
    //Check which command has been issued and do the appropriate action
    switch(control){
    case BT_STOP:
      rover->stop();
      goingForward = false;
      break;
    case BT_FORWARD:
      goingForward = true;
      if(!blockForward){
        rover->straight(1);
      }
      break;
    case BT_BACKWARD:
      rover->straight(-1);
      goingForward = false;
      break;
    case BT_LEFT:
    case BT_R_LEFT:
      rover->turn(true);
      goingForward = false;
      break;
    case BT_RIGHT:
    case BT_R_RIGHT:
      rover->turn(false);
      goingForward = false;
      break;
    default:
      break;
    }

    return true;
  }

  virtual String getKey(){
    return "BRC";
  }

  virtual long getInterval(){
    return 1;
  }
};

#endif
