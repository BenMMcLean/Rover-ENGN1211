#ifndef PORT_H
#define PORT_H

/**
 * Wraps the arduino's pins, so that they can be passed as an object, and
 * that, when used with ReadablePort and WritablePort it is known to the
 * object what can be done with it
 */
class Port{
public:
  int pin;

  Port(){}
  
  Port(int pin){
    this->pin = pin;
  }
};

#endif
