#ifndef MOTOR_H
#define MOTOR_H

/**
 * Provides a standard interface for interacting with motors
 */
class Motor{
public:
  //Sets the on/off state of the motor
  virtual void state(bool state) = 0;
  //Sets the speed of the motor. Negative numbers mean backwards
  virtual void speed(int speed) = 0;
  //Turns the motor a specified number of degrees
  virtual void turnDegrees(int degree) = 0;
};

#endif
