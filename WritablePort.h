#ifndef WRITABLEPORT_H
#define WRITABLEPORT_H

#include "Port.h"

/**
 * Allows a pin to be written to
 */
class WritablePort: public Port{
public:
  bool bstate = false;

  WritablePort(){}

  WritablePort(int pin) : Port(pin){
    pinMode(pin, OUTPUT);
  }

  WritablePort(int pin, bool state) : Port(pin){
    this->bstate = state;
    pinMode(pin, OUTPUT);
    digitalWrite(pin, state);
  }

  void state(bool state){
    this->bstate = state;
    digitalWrite(pin, state);
  }

  void toggle(){
    state(!bstate);
  }
};

#endif
