#ifndef HMOTOR_H
#define HMOTOR_H

#include "Motor.h"
#include "WritablePort.h"
#include "ReadablePort.h"

/**
 * Controls motors connected to a H-Bridge
 */
class HMotor : public Motor{
public:
  //Forwards motor controller
  WritablePort controlPort1;
  //Backwards motor controller
  WritablePort controlPort2;

  /*
   * The radius of the wheel the motor is attached to, used for
   * distance calculations
   */
  double wheelRadius = 1.5;

  HMotor(){}

  HMotor(WritablePort controlPort1, WritablePort controlPort2){
    this->controlPort1 = controlPort1;
    this->controlPort2 = controlPort2;
  }

  virtual void state(bool state){
    controlPort1.state(LOW);
    controlPort2.state(LOW);
  }

  virtual void speed(int speed){
    if(speed >= 0){
      controlPort1.state(HIGH);
      controlPort2.state(LOW);

      if(speed == 0){
        state(false);
      }
    }else if(speed < 0){
      controlPort1.state(LOW);
      controlPort2.state(HIGH);      
    }
  }

  virtual void turnDegrees(int deg){
    //TODO needs to be calibrated and calculated
  }
};

#endif
