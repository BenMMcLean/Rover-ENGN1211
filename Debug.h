#ifndef DEBUG_H
#define DEBUG_H

#include "Bluetooth.h"

/**
 * Handles debug messages
 */
class Debug{
private:
  //Determines if the debug message is sent over serial (true) or bluetooth (false)
  const static bool SERIAL_OUTPUT = true;
  
public:
  //The bluetooth module to output messages over
  static Bluetooth* output;

  //Print a message
  static void out(String msg){
    if(SERIAL_OUTPUT){
      Serial.println(msg);
    }else{
      output->write(msg);
      output->write("\n");
    }
  }

  //Print an error
  static void error(String msg){
    if(SERIAL_OUTPUT){
      Serial.print("!!! ");
      Serial.println(msg);
    }else{
      output->write("!!! ");
      output->write(msg);
      output->write("\n");
    }
  }
};

#endif
