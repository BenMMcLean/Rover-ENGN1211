#ifndef ROVERCONTROLLER_H
#define ROVERCONTROLLER_H

#include "Rover.h"
#include "Executable.h"

/**
 * Holds the business logic for the different rover modes
 */
class RoverController : public Executable{
public:
  Rover * rover;

  RoverController(){}
  RoverController(Rover * rover){
    this->rover = rover;
  }
};

#endif
