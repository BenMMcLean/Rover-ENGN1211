#include <Servo.h>
#include "BluetoothRoverController.h"
#include "TestRoverController.h"
#include "KnownRoverController.h"
#include "ViewController.h"
#include "RoverController.h"
#include "Rover.h"
#include "Motor.h"
#include "HMotor.h"
#include "Bluetooth.h"
#include "RoverMode.h"
#include "Debug.h"
#include "IUltrasonic.h"
#include "Ultrasonic.h"
#include "Port.h"
#include "ReadablePort.h"
#include "WritablePort.h"
#include "TaskScheduler.h"
#include "TS.h"

//As defined in wiring diagram
SoftwareBluetooth bluetooth(3,2,9600);
HMotor leftMotor = HMotor(WritablePort(A0),WritablePort(A1));
HMotor rightMotor = HMotor(WritablePort(A2),WritablePort(A3));
Servo ultrasonicServo;
IUltrasonic ultrasonic = IUltrasonic(WritablePort(11),ReadablePort(10));

Bluetooth* Debug::output = &bluetooth;

Rover rover = Rover(&leftMotor, &rightMotor, &ultrasonicServo, &ultrasonic);
RoverController* rc;

RoverMode roverMode = RoverMode::KNOWN;

void setup() {
  Serial.begin(9600);
  ultrasonicServo.attach(9);
  
  switch(roverMode){
    case TEST:
      rc = new TestRoverController(&rover);
      break;
    case CONTROL:
      rc = new BluetoothRoverController(&bluetooth, &rover);
      break;
    case KNOWN:
      rc = new KnownRoverController(&rover);
      break;
  }
}

void loop() {
  rc->run(millis());
  delay(10);
}
