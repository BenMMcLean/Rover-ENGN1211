#ifndef IULTRASONIC_H
#define IULTRASONIC_H

#include "Ultrasonic.h"
#include "Motor.h"
#include "WritablePort.h"
#include "ReadablePort.h"
#include "Debug.h"

/*
 * An implementation of the Ultrasonic interface
 */
class IUltrasonic : public Ultrasonic{
public:
  //Sends a pulse
  WritablePort trigPin;
  //Receives the pulse
  ReadablePort echoPin;

  IUltrasonic(WritablePort trigPin, ReadablePort echoPin){
    this->trigPin = trigPin;
    this->echoPin = echoPin;
  }

  //Gets the distance between the sensor and the next solid object
  virtual double getDistance(){
    return microsecondsToCentimeters(getDuration());
  }

  //Gets the average distance of multiple rounds of measuring
  virtual double getDistance(int rounds){
    long total = 0;
    for(int i = 0; i < rounds; i++){
      total+=getDuration();
    }
    return microsecondsToCentimeters(total/rounds);
  }

private:
  //Gets the duration of the ultrasonic ping in microseconds
  long getDuration(){
    trigPin.state(LOW);
    delayMicroseconds(2);
    trigPin.state(HIGH);
    delayMicroseconds(3);
    trigPin.state(LOW);

    return pulseIn(echoPin.pin, HIGH);
  }

  //Source: https://www.arduino.cc/en/Tutorial/Ping
  double microsecondsToCentimeters(long microseconds){
    return microseconds/29/2;
  }
};

#endif
