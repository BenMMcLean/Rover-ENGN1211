#ifndef EXECUTABLE_H
#define EXECUTABLE_H

#include "TaskScheduler.h"

class Executable{
public:
  virtual bool run(long offset) = 0;
  virtual String getKey() = 0;
  virtual long getInterval() = 0;
};

#endif
