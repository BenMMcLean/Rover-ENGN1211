#ifndef KNOWNROVERCONTROLLER_H
#define KNOWNROVERCONTROLLER_H

#include "Bluetooth.h"
#include "RoverController.h"
#include "Debug.h"

/*
 * This controller allows the user to move the rover using a bluetooth app
 */
class KnownRoverController : public RoverController{
public:
  KnownRoverController(){}
  KnownRoverController(Rover * r) : RoverController(r){
    Debug::out("Using Known Rover Controller");
  }

  virtual bool run(long diff){
    delay(5000);
    rover->straight(46, 1);
    rover->turnOne(118.0);
    
    rover->straight(65, 1);
    rover->turnOne(43.0);
    
    rover->straight(32.6,1);
    rover->turnOne(-56.0);
    rover->straight(27, 1);
    
    rover->turnOne(-50.0);
    rover->straight(34.6,1);
    rover->turnOne(48.0);

    rover->straight(43,1);
    rover->turnOne(118.0);
    rover->straight(70);
    return true;
  }

  virtual String getKey(){
    return "KRC";
  }

  virtual long getInterval(){
    return 1;
  }
};

#endif
