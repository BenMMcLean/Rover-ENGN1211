#ifndef ROVERMODE_H
#define ROVERMODE_H

enum RoverMode{
  CONTROL,
  KNOWN,
  UNKNOWN,
  TEST
};

#endif
