#ifndef BLUETOOTH_H
#define BLUETOOTH_H

#include <SoftwareSerial.h>

/*
 * Wraps the Serial and Software bluetooth modes, and provides a simplified interface
 * to the module
 */
class Bluetooth{
public:
  /*
   * Read the current buffer from the bluetooth module
   */
  virtual String read() = 0;

  /**
   * Read the first byte of the buffer
   */
  virtual byte readOne() = 0;
  
  /*
   * Write to the bluetooth module
   */
  virtual void write(String s) = 0;
};

/*
 * Software serial bluetooth implementation
 */
class SoftwareBluetooth : public Bluetooth{
public:
  SoftwareSerial io = SoftwareSerial(0,0);

  SoftwareBluetooth(){}

  /*
   * Takes in the receiving, transmitting, and board rate, and initlializes a software serial
   */
  SoftwareBluetooth(int rx, int tx, int rate){
    io = SoftwareSerial(rx,tx);
    io.begin(rate);
  }

  virtual String read(){
    String s;
    while(io.available()){
      s+=io.read();
    }
    return s;
  }

  virtual byte readOne(){
    if(io.available()){
      return io.read();
    }
    return '\0';
  }

  virtual void write(String s){
    io.write(s.c_str());
  }
};

#endif
