#ifndef ROVER_H
#define ROVER_H

#include <Servo.h>
#include "HMotor.h"
#include "Ultrasonic.h"
#include "TaskScheduler.h"
#include "Executable.h"
#include "TS.h"
#include "Debug.h"

/**
 * Represents the rover and its basic sensors, and provides boilerplate functions
 * for interacting with it
 */

class Rover{
private:
  const int DEFAULT_SCAN_ROUNDS = 3;
  
public:
  Motor * leftMotor;
  Motor * rightMotor;
  Servo * ultrasonicServo;
  Ultrasonic * ultrasonic;
  const double distanceBetweenWheels = 0;

  Rover(){}
  Rover(Motor * leftMotor, Motor * rightMotor, Servo * ultrasonicServo, Ultrasonic * ultrasonic){
    this->leftMotor = leftMotor;
    this->rightMotor = rightMotor;
    this->ultrasonicServo = ultrasonicServo;
    this->ultrasonic = ultrasonic;
  }

  //Stops the rover
  void stop(){
    leftMotor->speed(0);
    rightMotor->speed(0);
  }

  //Makes the rover go straight
  void straight(int speed){
    leftMotor->speed(speed);
    rightMotor->speed(speed);
  }

  void straight(double distance, int speed){
    straight(speed);

    long time = (5000.0/20.0)*distance;
    Debug::out(String(time));
    delay(time);

    stop();
  }

  //Turns the rover
  void turn(bool left){
    int s = (left) ? -1 : 1;
    leftMotor->speed(s);
    rightMotor->speed(-s);
  }

  //Turns the rover a specified number of degrees
  void turn(double degree){
    bool dir = false;
    if(degree < 0){
      dir = true;
      degree=-degree;
    }
    long time = (4700/180.0)*degree;
    Debug::out(String(time));

    turn(dir);
    delay(time);
    stop();
  }

  void turnOne(double degree){
    bool dir = false;
    if(degree < 0){
      dir = true;
      degree=-degree;
    }
    long time = (4700/180.0)*degree*2;

    if(dir){
      rightMotor->speed(1);
    }else{
      leftMotor->speed(1);
    }
    delay(time);
    stop();
  }

  //Scans a particular angle with the ultrasonic sensor
  double scan(int degree){
    return scan(degree, DEFAULT_SCAN_ROUNDS);
  }

  double scan(int degree, int rounds){
    ultrasonicServo->write(degree);
    return ultrasonic->getDistance(rounds);
  }
};


#endif
