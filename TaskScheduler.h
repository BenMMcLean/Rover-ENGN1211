#ifndef TASKSCHEDULER_H
#define TASKSCHEDULER_H

#include "Executable.h"

class Task{
public:
  Executable* exe;
  int nextRun;

  Task(Executable * exe){
    this->exe = exe;
    nextRun = exe->getInterval();
  }
};

/**
 * This class facilitates cooperative multitasking, allowing certain functions to be run at a specified time
 */
class TaskScheduler{
  Task* tasks;
  int size;
  
public:
  TaskScheduler(){
    tasks = (Task*)malloc(0);
    size = 0;
  }

  ~TaskScheduler(){
    for(int i = 0; i < size; i++){
      delete tasks[i].exe;
    }
    free(tasks);
  }

  void check(long time){
    for(int i = 0; i < size; i++){
      Task t = tasks[i];
      if(t.nextRun <= time){
        long diff = time - t.nextRun;
        if(t.exe->run(diff)){
          t.nextRun = time + t.exe->getInterval() - diff;
        }else{
          remove(i);
          i--;
        }
      }
    }
  }
 
  void add(Executable * t){
    size++;
    tasks = (Task*)realloc(tasks, size * sizeof(Task));
    tasks[size-1] = Task(t);
  }

  void removeKey(String key){
    for(int i = 0; i < size; i++){
      if(tasks[i].exe->getKey() == key){
        remove(i);
        i--;
      }
    }
  }

private:
  void remove(int index){
    delete tasks[index].exe;
    size--;
    for(int i = index; i < size; i++){
      tasks[i] = tasks[i+1];
    }
    tasks = (Task*)realloc(tasks, size * sizeof(Task));
  }
};

#endif
