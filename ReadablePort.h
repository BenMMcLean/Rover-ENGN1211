#ifndef READABLEPORT_H
#define READABLEPORT_H

#include "Port.h"

/**
 * Allows a pin to be read
 */
class ReadablePort : public Port{
public:
  ReadablePort(){}

  ReadablePort(int pin) : Port(pin){
    pinMode(pin, INPUT);
  }

  bool read(){
    return digitalRead(pin);
  }
};

#endif
