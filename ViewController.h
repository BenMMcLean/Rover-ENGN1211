#ifndef VIEWCONTROLLER_H
#define VIEWCONTROLLER_H

#include "Rover.h"

class DistancePoint{
public:
  double angle;
  double distance;

  DistancePoint(){}
  DistancePoint(double angle, double distance){
    this->angle = angle;
    this->distance = distance;
  }
};

class ViewController{
  static const int scanDegInterval = 90;
  static const int numScans = (180/scanDegInterval)+1;
public:
  DistancePoint points[numScans];
  Rover * rover;

  ViewController(){}
  ViewController(Rover * rover){
    this->rover = rover;
  }

  void scan(){
    for(int i = 0; i < numScans; i++){
      int deg = i*scanDegInterval;
      points[i] = DistancePoint(deg,rover->scan(deg)); 
    }
  }

  DistancePoint* getAvailableDirections(){
    return points; //Temp
  }
  
};

#endif
