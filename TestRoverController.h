#ifndef TESTROVERCONTROLLER_H
#define TESTROVERCONTROLLER_H

class TestRoverController : public RoverController{
public:
  TestRoverController(Rover * r) : RoverController(r){}

  virtual bool run(long diff){
    delay(5000);
    rover->straight(10,1);
    //rover->turn(90.0);
    return false;
  }

  virtual String getKey(){
    return "TRC";
  }

  virtual long getInterval(){
    return 1;
  }
};

#endif
