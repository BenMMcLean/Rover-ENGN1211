#ifndef ULTRASONIC_H
#define ULTRASONIC_H

class Ultrasonic{
public:
  //Gets the distance as measured by the ultrasonic sensor
  virtual double getDistance() = 0;
  //Reads the distance from the ultrasonic sensor <rounds> times, returning the average
  virtual double getDistance(int rounds) = 0;
};

#endif
